// Copyright 2021 Klara Gicheva

#include <iostream>

int vrh = -1;
int rep = 0;
int glava = 0;
void push(int S[], int MAX) {
    int x;

    std::cout << " Vnesi element: ";
    std::cin >> x;
    vrh = vrh + 1;
    S[vrh] = x;

    if (vrh == MAX) {
        std::cout << " Polna niza! " << std::endl;
    } else {
        std::cout << " x = " << x << std::endl;
        std::cout << " S[" << vrh << "] = " << S[vrh] << std::endl;
        std::cout << std::endl;
    }
}
void pop(int S[]) {
    if (vrh == -1) {
        std::cout << " Prazna niza" << std::endl;
    } else {
        std::cout << " Zadnji element: ";
        std::cout << " S[" << vrh - 1 << "]= " << S[vrh - 1];
        std::cout << std::endl;
        vrh = vrh - 1;
    }
}
void izpis_sklada(int MAX, int S[]) {
    for (int k = 0; k < MAX - 1; k++) {
        std::cout << " S[" << k << "]= " << S[k] << std::endl;
    }
}
void vpisi_kroznavrsta(int Q[], int MAX, int x) {
    int novi_rep;
    novi_rep = (rep % MAX) + 1;
    if (glava == novi_rep) {
        std::cout << " Polna krozna vrsta" << std::endl;
    } else {
        std::cout << " Vpisi element: ";
        std::cin >> x;
        Q[rep] = x;
        rep = novi_rep;
        std::cout << "   " << Q[rep - 1] << std::endl;
        std::cout << std::endl;
    }
}
void beri_kroznavrsta(int Q[], int MAX) {
    int x;
    if (glava == rep) {
        std::cout << " Prazna krozna vrsta" << std::endl;
    } else {
        x = Q[glava];
        glava = (glava % MAX) + 1;
        for (int t = 1; t < MAX; t++) {
            std::cout << "   " << Q[t - 1] << std::endl;
        }
    }
}
void izpis_kroznavrsta(int Q[], int MAX) {
    for (int d = 1; d < MAX; d++) {
        std::cout << "   " << Q[d] << std::endl;
    }
}

int main() {
    int MAX;
    int izbir;
    std::cout << " Velikosti vrste: ";
    std::cin >> MAX;
    int x;
    int S[MAX];
    int Q[MAX];
    do {
        std::cout << "\n Sklad - izbira: " << std::endl;
        std::cout << " 1) Vnos podatka \n ";
        std::cout << " 2) Branje podatka \n ";
        std::cout << "3) Izpis vsebine sklada \n " << std::endl;
        std::cout << " Krozna vrsta - izbira: " << std::endl;
        std::cout << " 4) Vnos podatka \n ";
        std::cout << " 5) Branje podatka \n ";
        std::cout << " 6) Izpis vrste od glave do repa \n " << std::endl;
        std::cout << " 7) Konec \n" << std::endl;
        std::cout << " Vnesi izbir: ";
        std::cin >> izbir;
        std::cout << std::endl;
        if (izbir == 1) {
            for (int i = 0; i <= MAX; i++) {
                push(S, MAX);
            }
        } else if (izbir == 2) {
            pop(S);
        } else if (izbir == 3) {
            izpis_sklada(MAX, S);
        } else if (izbir == 4) {
            for (int j = 0; j <= MAX; j++) {
                vpisi_kroznavrsta(Q, MAX, x);
            }
        } else if (izbir == 5) {
            beri_kroznavrsta(Q, MAX);
        } else if (izbir == 6) {
            izpis_kroznavrsta(Q, MAX);
        }
    } while (izbir != 7);
    {
        std::cout << " \n Konec!! ";
    }
        return 0;
}
