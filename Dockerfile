ARG VERSION=20.04

FROM ubuntu:${VERSION}
FROM gcc:latest
FROM sharaku/cpplint:latest

COPY . /usr/src/cpp_test

WORKDIR /usr/src/cpp_test

RUN g++ -o helloworld aps.cpp

CMD [ "cpplint", "aps.cpp" ]
